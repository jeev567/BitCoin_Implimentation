package com.superman.main;

import java.util.Date;

import com.superman.block.Block;
import com.superman.block.BlockChain;
import com.superman.utility.Utility;

public class App {
	public static void main(String[] args) {
		
		
		//System.out.println(Utility.getHash("0"));
		//System.out.println(Utility.getHash("00"));
		//System.out.println(Utility.getHash("000"));
		System.out.println(Utility.getHash("00000"));
		
		BlockChain jeeCoin = new BlockChain();
		System.out.println("Mining Block 6");
		jeeCoin.addBlock(new Block(new Date(),"Block 1", jeeCoin.getChain().get(jeeCoin.getChain().size()-1).getHash()));
		System.out.println("Mining Block 5");
		jeeCoin.addBlock(new Block(new Date(),"Block 2", jeeCoin.getChain().get(jeeCoin.getChain().size()-1).getHash()));
		System.out.println("Mining Block 4");
		jeeCoin.addBlock(new Block(new Date(),"Block 3", jeeCoin.getChain().get(jeeCoin.getChain().size()-1).getHash()));
		System.out.println("Mining Block 3");
		jeeCoin.addBlock(new Block(new Date(),"Block 4", jeeCoin.getChain().get(jeeCoin.getChain().size()-1).getHash()));
		System.out.println("Mining Block 2");
		jeeCoin.addBlock(new Block(new Date(),"Block 5", jeeCoin.getChain().get(jeeCoin.getChain().size()-1).getHash()));
		
		
		System.out.println(jeeCoin.toString());
		System.out.println("Before Force Inserting..Is chain valid: "+jeeCoin.isChainValid());

		
		Block k = jeeCoin.getChain().get(0);
		k.setData("Hi");
		System.out.println("After Force Inserting..Is Chain Valid: "+jeeCoin.isChainValid());
		
		
	
		
		
	}
}
