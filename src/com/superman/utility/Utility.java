package com.superman.utility;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.Hashing;

public class Utility {
	public static String getHash(String originalString){
		return Hashing.sha256().hashString(originalString, StandardCharsets.UTF_8)
				.toString();
	}
	
	
	public static String mineBlock(int difficulty,String hash){
		String k="0";
		for (int i = 0; i < difficulty-1; i++) {
			k=k+"0";
		}
		//Hash for genesis block is 0 and a substrip will give exception
		while(!hash.equals("0") && !hash.substring(0,difficulty).equals(k)){
			hash =getHash(hash);
		}
		System.out.println("Block Mined..."+ hash);
		return hash;
	}
	
}
