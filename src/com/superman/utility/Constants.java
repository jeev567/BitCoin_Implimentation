package com.superman.utility;

public interface Constants {
	public static final String GENESIS_BLOCK = "Genesis Block";
	public static final String GENESIS_BLOCK_VALUE = "0";
	public static final String BLOCK_VARIABLE_TIMESTAMP = "Timsstamp";
	public static final String BLOCK_VARIABLE_DATA = "Data";
	public static final String BLOCK_VARIABLE_PREV_HASH = "Prev Hash";
	public static final String BLOCK_VARIABLE_HASH = "Hash";
	public static final String BLOCK_VARIABLE_BLOCKCHAIN = "BlockChain";
	
	
	
	public static final int BLOCK_CHAIN_DIFFICULTY_LEVEL = 4;
}
