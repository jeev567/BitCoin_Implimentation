package com.superman.block;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.superman.utility.Constants;
import com.superman.utility.Utility;

public class BlockChain {
	
	List<Block> chain = null;

	public List<Block> getChain() {
		return chain;
	}

	public void setChain(List<Block> chain) {
		this.chain = chain;
	}

	public BlockChain() {
		chain = new ArrayList<>();
		chain.add(GenesisBlock());

	}

	private Block GenesisBlock() {
		return new Block(new Date(), Constants.GENESIS_BLOCK, Constants.GENESIS_BLOCK_VALUE);
	}

	private Block getLatestBlock() {
		return this.chain.get(chain.size() - 1);
	}

	public void addBlock(Block newBlock) {
		newBlock.setPrevHash(Utility.mineBlock(Constants.BLOCK_CHAIN_DIFFICULTY_LEVEL,this.getLatestBlock().getHash()));
		newBlock.setHash(Utility.mineBlock(Constants.BLOCK_CHAIN_DIFFICULTY_LEVEL,newBlock.getHash()));
		chain.add(newBlock);
	}

	public boolean isChainValid() {
		for (int i = 1; i < chain.size(); i++) {
			Block currentBlock = chain.get(i);
			Block previosBlock = chain.get(i - 1);
			if (!(currentBlock.getHash().equals(Utility.mineBlock(Constants.BLOCK_CHAIN_DIFFICULTY_LEVEL,Utility.getHash(currentBlock.getTimestamp()+ currentBlock.getData()+ currentBlock.getPrevHash()))))) {
				return false;
			}
			if (!currentBlock.getPrevHash().equals(Utility.mineBlock(Constants.BLOCK_CHAIN_DIFFICULTY_LEVEL,Utility.getHash(previosBlock.getTimestamp()+ previosBlock.getData()+ previosBlock.getPrevHash())))) {
				return false;
			}
		}

		return true;
	}

	

	@Override
	public String toString() {
		JSONObject json = null;
		JSONObject wrapper = new JSONObject();
		JSONArray array = new JSONArray();
		for (Block block : chain) {
			json = new JSONObject();
			json.put(Constants.BLOCK_VARIABLE_TIMESTAMP, block.getTimestamp().toString());
			json.put(Constants.BLOCK_VARIABLE_DATA, block.getData());
			json.put(Constants.BLOCK_VARIABLE_PREV_HASH, block.getPrevHash());
			json.put(Constants.BLOCK_VARIABLE_HASH, block.getHash());
			array.put(json);
		}
		wrapper.put(Constants.BLOCK_VARIABLE_BLOCKCHAIN, array);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(wrapper.toString());
		return gson.toJson(je);
	}

}