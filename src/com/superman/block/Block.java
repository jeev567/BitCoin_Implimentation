package com.superman.block;

import java.util.Date;

import com.superman.utility.Utility;

/**
 * 
 * @author jeev567
 *
 */
public class Block {
	private Date timestamp;
	private String data;
	private String prevHash;
	private String hash;
	
	public Block(Date timestamp, String data, String prevHash) {
		this.timestamp = timestamp;
		this.data = data;
		this.prevHash = prevHash;
		this.hash = Utility.mineBlock(4, Utility.getHash(this.timestamp + this.data + this.prevHash));
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getPrevHash() {
		return prevHash;
	}

	public void setPrevHash(String prevHash) {
		this.prevHash = prevHash;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
